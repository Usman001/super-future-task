import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import postsReducer from "redux/postSlice/post.reducers";

/*************************************************/

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["auth"],
};

const rootReducer = combineReducers({
  postsSlice: postsReducer,
});

/*************************************************/

export default persistReducer(persistConfig, rootReducer);

/*************************************************/
