import { takeLatest, put, all, call } from 'redux-saga/effects';

import ApiCollections from 'configs/services/apiCollections'

import PostsActionTypes from 'redux/postSlice/post.types';
import * as PostsActions from './post.actions';


/*************************************************/

export function* getPosts({ payload }) {
  yield put(PostsActions.getPostsLoadingStart());
  try {
    const response = yield call(ApiCollections.getPosts, payload);

    yield put(PostsActions.getPostsSuccess(response));
    yield put(PostsActions.getPostsLoadingStop());
    payload.callback && payload.callback(response);
  } catch (error) {
    yield put(PostsActions.getPostsFailure(error));
    yield put(PostsActions.getPostsLoadingStop());
  }
}

/*************************************************/

export function* deletePost({ payload }) {
  yield put(PostsActions.deletePostLoadingStart());
  try {
    const response = yield call(ApiCollections.deletePost, payload);

    yield put(PostsActions.deletePostSuccess(response));
    yield put(PostsActions.deletePostLoadingStop());
    payload.callback && payload.callback(response);
  } catch (error) {
    yield put(PostsActions.deletePostFailure(error));
    yield put(PostsActions.deletePostLoadingStop());
  }
}

/*************************************************/

export function* getPostDetails({ payload }) {
  yield put(PostsActions.getPostDetailsLoadingStart());
  try {
    const response = yield call(ApiCollections.getPostDetails, payload);

    yield put(PostsActions.getPostDetailsSuccess(response));
    yield put(PostsActions.getPostDetailsLoadingStop());
    payload.callback && payload.callback(response);
  } catch (error) {
    yield put(PostsActions.getPostDetailsFailure(error));
    yield put(PostsActions.getPostDetailsLoadingStop());
  }
}

/*************************************************/

export function* createPost({ payload }) {
  yield put(PostsActions.createPostLoadingStart());
  try {
    const response = yield call(ApiCollections.createPost, payload);

    yield put(PostsActions.createPostSuccess(response));
    yield put(PostsActions.createPostLoadingStop());
    payload.callback && payload.callback(response);
  } catch (error) {
    yield put(PostsActions.createPostFailure(error));
    yield put(PostsActions.createPostLoadingStop());
  }
}

/*************************************************/

export function* updatePost({ payload }) {
  yield put(PostsActions.updatePostLoadingStart());
  try {
    const response = yield call(ApiCollections.updatePost, payload);

    yield put(PostsActions.updatePostSuccess(response));
    yield put(PostsActions.updatePostLoadingStop());
    payload.callback && payload.callback(response);
  } catch (error) {
    yield put(PostsActions.updatePostFailure(error));
    yield put(PostsActions.updatePostLoadingStop());
  }
}

/*************************************************/

export function* postsSagas() {
  yield all([
    yield takeLatest(PostsActionTypes.GET_POSTS_REQUEST, getPosts),
    yield takeLatest(PostsActionTypes.POST_DELETE_REQUEST, deletePost),
    yield takeLatest(PostsActionTypes.POST_DETAILS_REQUEST, getPostDetails),
    yield takeLatest(PostsActionTypes.POST_CREATE_REQUEST, createPost),
    yield takeLatest(PostsActionTypes.POST_UPDATE_REQUEST, updatePost),
  ]);
}

/*************************************************/