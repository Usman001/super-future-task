import PostActionTypes from 'redux/postSlice/post.types';

// Products
export const getPostsRequest = payload => ({
  type: PostActionTypes.GET_POSTS_REQUEST,
  payload: payload
});

export const getPostsSuccess = list => ({
  type: PostActionTypes.GET_POSTS_SUCCESS,
  payload: list
});

export const getPostsFailure = error => ({
  type: PostActionTypes.GET_POSTS_FAILURE,
  payload: error
});

export const getPostsLoadingStart = () => ({
  type: PostActionTypes.GET_POSTS_LOADING_START,
});

export const getPostsLoadingStop = () => ({
  type: PostActionTypes.GET_POSTS_LOADING_STOP,
});

// -------------------------------------------------

// Delete Product

export const deletePostRequest = payload => ({
  type: PostActionTypes.POST_DELETE_REQUEST,
  payload: payload
});

export const deletePostSuccess = post => ({
  type: PostActionTypes.POST_DELETE_SUCCESS,
  payload: post
});

export const deletePostFailure = error => ({
  type: PostActionTypes.POST_DELETE_FAILURE,
  payload: error
});

export const deletePostLoadingStart = () => ({
  type: PostActionTypes.POST_DELETE_LOADING_START,
});

export const deletePostLoadingStop = () => ({
  type: PostActionTypes.POST_DELETE_LOADING_STOP,
});

// -------------------------------------------------

// Create Product

export const createPostRequest = payload => ({
  type: PostActionTypes.POST_CREATE_REQUEST,
  payload: payload
});

export const createPostSuccess = post => ({
  type: PostActionTypes.POST_CREATE_SUCCESS,
  payload: post
});

export const createPostFailure = error => ({
  type: PostActionTypes.POST_CREATE_FAILURE,
  payload: error
});

export const createPostLoadingStart = () => ({
  type: PostActionTypes.POST_CREATE_LOADING_START,
});

export const createPostLoadingStop = () => ({
  type: PostActionTypes.POST_CREATE_LOADING_STOP,
});

// -------------------------------------------------

// Update Product

export const updatePostRequest = payload => ({
  type: PostActionTypes.POST_UPDATE_REQUEST,
  payload: payload
});

export const updatePostSuccess = post => ({
  type: PostActionTypes.POST_UPDATE_SUCCESS,
  payload: post
});

export const updatePostFailure = error => ({
  type: PostActionTypes.POST_UPDATE_FAILURE,
  payload: error
});

export const updatePostLoadingStart = () => ({
  type: PostActionTypes.POST_UPDATE_LOADING_START,
});

export const updatePostLoadingStop = () => ({
  type: PostActionTypes.POST_UPDATE_LOADING_STOP,
});

// -------------------------------------------------

// Product Details

export const getPostDetailsRequest = payload => ({
  type: PostActionTypes.POST_DETAILS_REQUEST,
  payload: payload
});

export const getPostDetailsSuccess = post => ({
  type: PostActionTypes.POST_DETAILS_SUCCESS,
  payload: post
});

export const getPostDetailsFailure = error => ({
  type: PostActionTypes.POST_DETAILS_FAILURE,
  payload: error
});

export const getPostDetailsLoadingStart = () => ({
  type: PostActionTypes.POST_DETAILS_LOADING_START,
});

export const getPostDetailsLoadingStop = () => ({
  type: PostActionTypes.POST_DETAILS_LOADING_STOP,
});
