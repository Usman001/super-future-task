import ApiService from ".";

const BASE_URL = () => {
  if (process.env.REACT_APP_ENV === "development")
    return process.env.REACT_APP_BASE_URL_DEVELOPMENT;
  else if (process.env.REACT_APP_ENV === "staging")
    return process.env.REACT_APP_BASE_URL_STAGING;
  else if (process.env.REACT_APP_ENV === "production") {
    return process.env.REACT_APP_BASE_URL_PRODUCTION;
  }
};

const client = new ApiService({ baseURL: BASE_URL(), loginType: "user" });

const ApiCollections = {
  getPosts: (params) =>
    client.get(`posts?_start=${params.start}&_limit=${params.limit}`),
  deletePost: (params) => client.delete(`posts/${params.id}`),

  getPostDetails: (params) => client.get(`posts/${params.id}`),
  createPost: (params) => client.imgPost("posts", params.formData),
  updatePost: (params) =>
    client.patch(`posts/${params.formData.id}`, params.formData),
};
export default ApiCollections;
