import { useSelector } from "react-redux";
import moment from 'moment';
import { FILE_BASE_URL, FILE_BASE_URL_S3 } from 'configs/services/apiCollections';

export const GetTrainerToken = () => {
    const Authorization = useSelector(state => state.auth.trainer_token);
    if (Authorization.length > 0) return Authorization
    else return false
}

export const GetUserToken = () => {
    const Authorization = useSelector(state => state.auth.token);
    if (Authorization.length > 0) return Authorization
    else return false
}
