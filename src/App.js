import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// Routes
import * as routes from "routes/routes";
import CustomRoute from "routes/customRoute";

// Pages
import Posts from "pages/posts/posts";
import CreatePost from "pages/posts/create-post";
import UpdatePost from "pages/posts/update-post";

const App = () => {
  return (
    <div>
      <Router>
        <Switch>
          <CustomRoute path={routes.CREATE_POST} exact component={CreatePost} />
          <CustomRoute path={routes.UPDATE_POST} exact component={UpdatePost} />
          <CustomRoute path={routes.POSTS} exact component={Posts} />
          <Route path="/" render={() => "Route Not Found"} />
        </Switch>
      </Router>
    </div>
  );
};

export default App;
