export const ROOT = "/";
export const CREATE_POST = "/posts/create";
export const UPDATE_POST = "/posts/:id";
export const POSTS = "/posts";