import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

// Components
import { Input, Card, Alert } from "antd";
// Redux
import * as PostsActions from "redux/postSlice/post.actions";

const Posts = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [page, setPage] = useState(null);
  const [selectedPage, setSelectedPage] = useState(0);
  const [prevPage, setPrevPage] = useState(0);
  const [nextPage, setNextPage] = useState(0);
  const [selectedPost, setSelectedPosts] = useState(null);

  const isPostsLoading = useSelector(
    (state) => state.postsSlice.isPostsLoading
  );
  const isPostDeleting = useSelector(
    (state) => state.postsSlice.isPostDeleting
  );
  const deletedPost = useSelector((state) => state.postsSlice.posts);
  const posts = useSelector((state) => state.postsSlice.posts);

  useEffect(() => {
    getPosts(0, 10);
    setSelectedPage(1);
    setPage({
      currentPage: 1,
      previousPage: 0,
      nextPage: 2,
      startRange: 0,
      endRange: 10,
      totalPages: 10,
      lastPage: 10,
    });
  }, []);

  const deletePost = (post) => {
    setSelectedPosts(post);
    dispatch(
      PostsActions.deletePostRequest({
        id: post?.id,
        callback: () => {
          getPosts(0, 10);
          setSelectedPosts(null);
        },
      })
    );
  };

  const getPosts = (start, limit) => {
    dispatch(
      PostsActions.getPostsRequest({
        start,
        limit,
        callback: () => {
          setPage({ ...page, totalPages: posts.length / 10 });
        },
      })
    );
  };

  const changePage = (index) => {
    let currentPage = selectedPage;
    let startRange = page?.startRange;
    let endRange = page?.endRange;

    if (index === "Previous") {
      currentPage = selectedPage - 1;
      setSelectedPage(currentPage);
      endRange = currentPage * 10;
      startRange = endRange - 10;
      setPrevPage(selectedPage - 1);
      setNextPage(selectedPage + 1);
      setPage({
        ...page,
        currentPage,
        previousPage: index - 1,
        nextPage: index + 1,
        startRange,
        endRange,
      });
      getPosts(startRange, 10);
      return;
    }
    if (index === "Next") {
      currentPage = selectedPage + 1;
      setSelectedPage(currentPage);
      endRange = currentPage * 10;
      startRange = endRange - 10;
      setPrevPage(selectedPage - 1);
      setNextPage(selectedPage + 1);
      setPage({
        ...page,
        currentPage,
        previousPage: index - 1,
        nextPage: index + 1,
        startRange,
        endRange,
      });
      getPosts(startRange, 10);
      return;
    }

    currentPage = index;
    endRange = currentPage * 10;
    startRange = endRange - 10;
    setSelectedPage(index);
    setPrevPage(index - 1);
    setNextPage(index + 1);
    setPage({
      ...page,
      currentPage: index,
      previousPage: index - 1,
      nextPage: index + 1,
      startRange,
      endRange,
    });
    getPosts(startRange, 10);
  };

  return (
    <div className="container mt-5">
      <h1>Posts</h1>
      <div className="posts">
        {isPostsLoading && <h1 className="loader">Loading...</h1>}
        {!isPostsLoading && (
          <>
            {posts?.slice(page?.startRange, page?.endRange)?.map((post) => (
              <div className="post" key={post?.id}>
                <Card title={"Post ID: " + post?.id}>
                  <h4>
                    <b>Title:</b> {post?.title}
                  </h4>
                  <p>
                    <b>Description:</b> {post?.body}
                  </p>
                  <div className="actions-wrapper">
                    <button
                      className="btn-danger"
                      onClick={() => deletePost(post)}
                      disabled={selectedPost?.id === post?.id && isPostDeleting}
                    >
                      {selectedPost?.id === post?.id && isPostDeleting
                        ? "Deleting..."
                        : "Delete"}
                    </button>
                    <button
                      className="btn-primary"
                      onClick={() => {
                        history.push(`posts/${post?.id}`);
                      }}
                    >
                      Update
                    </button>
                  </div>
                </Card>
              </div>
            ))}
          </>
        )}
      </div>

      <div className="pagination-wrapper">
        <div className="pagination">
          <button
            className="btn-primary"
            disabled={selectedPage <= 1}
            onClick={() => changePage("Previous")}
          >
            Previous
          </button>
          {new Array(10).fill(page?.totalPages)?.map((j, i) => {
            const index = ++i;
            return (
              <button
                className={
                  index === selectedPage ? "active btn-index" : "none btn-index"
                }
                key={i}
                onClick={() => changePage(index)}
              >
                {index}
              </button>
            );
          })}
          <button
            className="btn-primary"
            disabled={selectedPage >= page?.lastPage}
            onClick={() => changePage("Next")}
          >
            Next
          </button>
        </div>
        <br />
        <br />
        <br />
      </div>
    </div>
  );
};

export default Posts;
