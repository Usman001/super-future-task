import React, { useState, useEffect } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";

// Components
import { Input, Card, Alert } from "antd";
// Redux
import * as PostsActions from "redux/postSlice/post.actions";
const { TextArea } = Input;

const UpdatePost = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();
  const [isUpdated, setIsUpdated] = useState(false);

  const isPostUpdating = useSelector(
    (state) => state.postsSlice.isPostUpdating
  );
  const updatePost = useSelector((state) => state.postsSlice.updatePost);
  const postDetails = useSelector((state) => state.postsSlice.postDetails);

  useEffect(() => {
    getPostDetails(id);
  }, []);

  const initialValues = {
    title: postDetails?.title,
    body: postDetails?.body,
    userId: postDetails?.userId,
  };

  const validationSchema = Yup.object().shape({
    title: Yup.string().required("Required"),
    body: Yup.string().required("Required"),
    userId: Yup.string().required("Required"),
  });

  const onFormSubmit = (values, { resetForm }) => {
    dispatch(
      PostsActions.updatePostRequest({
        formData: {
          title: values.title,
          body: values.body,
          userId: values.userId,
          id,
        },
        callback: () => {
          resetForm();
          setIsUpdated(true);
          getPostDetails(id);
          const timer = setTimeout(() => {
            setIsUpdated(false);
            clearTimeout(timer);
          }, 5000);
        },
      })
    );
  };

  const getPostDetails = (id) => {
    dispatch(
      PostsActions.getPostDetailsRequest({
        id,
        callback: () => {},
      })
    );
  };

  return (
    <>
      <div className="container mt-5">
        <Card title="Update Post" bordered={false}>
          {isUpdated && (
            <>
              <Alert message="Post has updated" type="success" />
              <br />
            </>
          )}
          <Formik
            enableReinitialize
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onFormSubmit}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              values,
              touched,
              errors,
              isValid,
              dirty,
            }) => (
              <form noValidate autoComplete="off" onSubmit={handleSubmit}>
                <Input
                  type="number"
                  placeholder="User ID"
                  name="userId"
                  onChange={handleChange}
                  value={values.userId}
                  status={errors.userId ? "error" : ""}
                  disabled
                />
                <p className="validation-text">{errors.userId}</p>
                <Input
                  type="text"
                  placeholder="Title"
                  name="title"
                  onChange={handleChange}
                  value={values.title}
                  status={errors.title ? "error" : ""}
                />
                <p className="validation-text">{errors.title}</p>
                <TextArea
                  rows={4}
                  type="text"
                  placeholder="Body"
                  name="body"
                  onChange={handleChange}
                  value={values.body}
                  status={errors.body ? "error" : ""}
                />
                <p className="validation-text">{errors.body}</p>

                <button
                  className="btn-primary"
                  type="submit"
                  disabled={!isValid || isPostUpdating}
                >
                  {isPostUpdating ? "Updating..." : "Submit"}
                </button>
              </form>
            )}
          </Formik>
        </Card>
      </div>
    </>
  );
};

export default UpdatePost;
