## Foodiary ReactJS Dashboard

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=applaunch_IN_foodiary-reactjs-dashboard&token=1841da3761d8e6595b8399e6031d7461fee52ac6)](https://sonarcloud.io/dashboard?id=applaunch_IN_foodiary-reactjs-dashboard)

#### Description:

NodeJS, NPM Installation and GIT needs to be installed in your system to run the application.

- Foodiary is health related dashboard will supply health supplements and food related queries.
- Dashboard UI Designing completed.
- API Integration has been completed.

## Project Status

- Completed
- Currently Working on Client's Feedback and modifications.

## Installed Packages

- [react-router-dom](https://www.npmjs.com/package/react-router-dom) - For formatting the time and timestamp data based on our requirement easily and quickly.
- [react-icons](https://www.npmjs.com/package/react-icons) - To use free svg icons which are available by different distributors.
- [react-redux](https://www.npmjs.com/package/react-redux) - To link redux with React Components.
- [redux](https://www.npmjs.com/package/redux) - To manage the state of the application.
- [styled-components](https://www.npmjs.com/package/styled-components) - To style the components with styled components without any css file.
- [formik](https://www.npmjs.com/package/formik) - To handle form states and make the from control easy.
- [yup](https://www.npmjs.com/package/yup) - Validate the form data with advanced and validate the form input data.
- [axios](https://www.npmjs.com/package/axios) - For easy API requests.
- [material-ui-time-picker](https://www.npmjs.com/package/material-ui-time-picker) - Time picker Component by material UI.
- [moment](https://www.npmjs.com/package/moment) - For formatting the time and timestamp data based on our requirement easily and quickly.
- [react-multi-carousel](https://www.npmjs.com/package/react-multi-carousel) - Production-ready, lightweight fully customizable React carousel component that rocks supports multiple items and SSR.
- [react-spinners](https://www.npmjs.com/package/react-spinners) - To add spinner while the data is loading or during api request.
- [redux-devtools-extension](https://www.npmjs.com/package/redux-devtools-extension) - Best tool while development with redux.
- [redux-logger](https://www.npmjs.com/package/redux-logger) - To log all the changes made in the redux and log each stage change in dev mode to check the state is updating or not.
- [redux-persist](https://www.npmjs.com/package/redux-persist) - Save redux store locally and make it Persist and rehydrate a redux store.
- [redux-saga](https://www.npmjs.com/package/redux-saga) - An intuitive Redux side effect manager. Easy to manage, easy to test, and executes efficiently.
- [@material-ui/core](https://www.npmjs.com/package/@material-ui/core) - For Adding Ready made components easily without much styling.

## Installation and Setup Instructions

Clone down this repository. You will need `node` and `npm` installed globally on your machine.
You can use `yarn` too for the setup.

Installation:

`npm install` | `yarn`

To Run Test Suite:

`npm test` | `yarn test`

To Start Server:

`npm start` | `yarn start`

To Visit App:

`localhost:3000`

## Reflection

- Some Screens needed client opinion and clarifications.
- Doing the Line Graph, Pie Chart, line Progress, linear Dragger, Donut Chart was quite challenging as needed to manipulate svg image based on the input.
